package lk.apiit.eirls.bangersandcoapi.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "userdocuments")
public class UserDocuments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "useremail",referencedColumnName = "email")
    private User user;

    private String fileName;

    private String fileType;

    @Lob
    private byte[] fileData;

    @CreationTimestamp
    private Date uploadedDate;

    private Date validTill;

    private String docTitle;

    private String docNo;
    private String url;


}

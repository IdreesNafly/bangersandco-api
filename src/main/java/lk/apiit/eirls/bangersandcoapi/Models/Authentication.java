package lk.apiit.eirls.bangersandcoapi.Models;

import lk.apiit.eirls.bangersandcoapi.Enums.UserRole;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "authentication")
public class Authentication {

    @Id
    @Column(name="email",unique = true,nullable = true)
    private String email;

    @Column(name="password")
    private String password;

    @Column(name = "userRole")
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @Column(name = "lastLogin")
    private Date lastLogin;

    @Column(name = "jwtToken")
    private String jwtToken;

    public Authentication(String email,String jwtToken){
        this.email=email;
        this.jwtToken=jwtToken;
    }

    public Authentication(String email, UserRole userRole) {
        this.email = email;
        this.userRole = userRole;
    }
}

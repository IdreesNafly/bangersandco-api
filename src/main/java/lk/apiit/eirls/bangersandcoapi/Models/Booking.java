package lk.apiit.eirls.bangersandcoapi.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lk.apiit.eirls.bangersandcoapi.Enums.BookingStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date bookingDate;

    private Date returnDate;

    private Date extendedDate;

    @Enumerated(EnumType.STRING)
    private BookingStatus bookingStatus;

    @ManyToOne
    @JoinColumn(name = "vehicleID")
    private Vehicle vehicle;

    @OneToMany(cascade = CascadeType.ALL)
    private List <SpecialEquipment> specialEquipment;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid")
    private User user;


    public Booking(Date bookingDate, Date returnDate, BookingStatus bookingStatus, Vehicle vehicle, List<SpecialEquipment> specialEquipment, User user) {
        this.bookingDate = bookingDate;
        this.returnDate = returnDate;
        this.bookingStatus = bookingStatus;
        this.vehicle = vehicle;
        this.specialEquipment = specialEquipment;
        this.user = user;
    }
}

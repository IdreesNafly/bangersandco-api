package lk.apiit.eirls.bangersandcoapi.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="user")
public class User {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "email",referencedColumnName = "email")
    @MapsId("email")
    private Authentication authentication;

    @Id
    @Column(unique = true,nullable = false)
    private String email;

    private String firstName;

    private String lastName;

    private String gender;

    private String address;

    private String contactNo;

    private boolean isNewUser=true;

    private boolean isBacklisted;

    private String age;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private List <UserDocuments> userDocuments;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List <Booking> bookings;

    public User(Authentication authentication, String email) {
        this.authentication = authentication;
        this.email = email;
    }
}

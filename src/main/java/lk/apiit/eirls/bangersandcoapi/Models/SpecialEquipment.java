package lk.apiit.eirls.bangersandcoapi.Models;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "specialequipment")
public class SpecialEquipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int equipmentId;

    private String equipmentName;

    private Float costPerHour;

    private boolean availability=true;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bookingId", referencedColumnName = "id")
    private Booking booking;

    public SpecialEquipment(String equipmentName, Float costPerHour, boolean availability, Booking booking) {
        this.equipmentName = equipmentName;
        this.costPerHour = costPerHour;
        this.availability = availability;
        this.booking = booking;
    }

    public SpecialEquipment(String equipmentName, Float costPerHour) {
        this.equipmentName = equipmentName;
        this.costPerHour = costPerHour;
    }

    public SpecialEquipment(int equipmentId,Float costPerHour) {
        this.equipmentId=equipmentId;
        this.costPerHour = costPerHour;
    }

    public SpecialEquipment(Float costPerHour) {
        this.costPerHour = costPerHour;
    }
}

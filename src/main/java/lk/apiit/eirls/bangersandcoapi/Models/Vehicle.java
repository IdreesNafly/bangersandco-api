package lk.apiit.eirls.bangersandcoapi.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int vehicleID;

    private String vehicleType;

    private Float vehicleRate;


    private boolean availability=true;

    private String vehicleDescription;

    private String fuelType;

    private String transmission;

    private String mileage;

    private String imagePath;

    private String vehicleMake;

    private String vehicleModel;

    private String YOM;

//    @OneToMany(cascade = CascadeType.ALL)
//    private List<Booking> bookingList;

    public Vehicle(String vehicleType, Float vehicleRate, boolean availability, String vehicleDescription, String fuelType, String transmission, String mileage, String imagePath, String vehicleMake, String vehicleModel, String YOM) {
        this.vehicleType = vehicleType;
        this.vehicleRate = vehicleRate;
        this.availability = availability;
        this.vehicleDescription = vehicleDescription;
        this.fuelType = fuelType;
        this.transmission = transmission;
        this.mileage = mileage;
        this.imagePath = imagePath;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.YOM = YOM;
    }

    public Vehicle(String vehicleType, Float vehicleRate, boolean availability, String vehicleDescription, String fuelType, String transmission, String mileage, String imagePath, String vehicleMake, String vehicleModel, String YOM, List<Booking> bookingList) {
        this.vehicleType = vehicleType;
        this.vehicleRate = vehicleRate;
        this.availability = availability;
        this.vehicleDescription = vehicleDescription;
        this.fuelType = fuelType;
        this.transmission = transmission;
        this.mileage = mileage;
        this.imagePath = imagePath;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.YOM = YOM;
        //this.bookingList = bookingList;
    }

    public Vehicle(String vehicleType, Float vehicleRate, String vehicleDescription, String fuelType, String transmission, String mileage, String imagePath, String vehicleMake, String vehicleModel, String YOM) {
        this.vehicleType = vehicleType;
        this.vehicleRate = vehicleRate;
        this.vehicleDescription = vehicleDescription;
        this.fuelType = fuelType;
        this.transmission = transmission;
        this.mileage = mileage;
        this.imagePath = imagePath;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.YOM = YOM;
    }
}

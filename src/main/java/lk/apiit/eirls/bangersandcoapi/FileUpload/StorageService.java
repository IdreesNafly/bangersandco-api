package lk.apiit.eirls.bangersandcoapi.FileUpload;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.Date;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    String store(MultipartFile file, String email, String docno, String doctitle, Date expiryDate);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

    ResponseEntity<?> getAllDocumentsByUser(String email);
}

package lk.apiit.eirls.bangersandcoapi.FileUpload;

import lk.apiit.eirls.bangersandcoapi.Models.UserDocuments;
import lk.apiit.eirls.bangersandcoapi.Repositories.UserDocumentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.stream.Collectors;

@RestController
public class FilesUploadController {

    private StorageService storageService;

    @Autowired
    private UserDocumentRepo userDocumentRepo;

    @Autowired
    public FilesUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("/upload-file")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file,  @RequestParam("user") String email ,@RequestParam("doctitle") String doctitle, @RequestParam("docno") String docno, @RequestParam("expirydate") Date expiryDate) {

        String name = storageService.store(file, email, docno, doctitle, expiryDate);
        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(name)
                .toUriString();

        FileResponse fileResponse=new FileResponse(name, uri, file.getContentType(), file.getSize());
        return new ResponseEntity<>(fileResponse, HttpStatus.OK);
    }
//    @PostMapping("/upload-multiple-files")
//    @ResponseBody
//    public List<FileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files,@RequestParam("user") String email) {
//        return Arrays.stream(files)
//                .map(file -> uploadFile(file,email))
//                .collect(Collectors.toList());
//    }


    @GetMapping("/")
    public String listAllFiles(Model model) {

        model.addAttribute("files", storageService.loadAll().map(
                path -> ServletUriComponentsBuilder.fromCurrentContextPath()
                        .path("/download/")
                        .path(path.getFileName().toString())
                        .toUriString())
                .collect(Collectors.toList()));

        return "listFiles";
    }


    @GetMapping("/download/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) {

        Resource resource = storageService.loadAsResource(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }


    @GetMapping("/download/{fileName:.+}/db")
    public ResponseEntity downloadFromDB(@PathVariable String fileName) {
        UserDocuments document = userDocumentRepo.findByFileName(fileName);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(document.getFileData());
    }


    @GetMapping("/getdocuments/{email}")
    public ResponseEntity<?> getAllDocbyUser(@PathVariable String email){
        return storageService.getAllDocumentsByUser(email);
    }

}

package lk.apiit.eirls.bangersandcoapi.EmailService;

import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

@Service
public class EmailService {

    EmailConfig emailConfiguration;

    public void sendEmail(EmailModel email) {

        final String username = "idreesnafly@gmail.com";
        final String password = "idreesnafly";
//        final String username = emailConfiguration.getSenderEmail();
//        final String password = emailConfiguration.getSenderPassword();

        Properties props = new Properties();
        props.setProperty("mail.user","idreesnafly@gmail.com");
        props.setProperty("mail.password","idreesnafly");
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email.getEmail()));
            message.setSubject(email.getSubject());
            //message.setText(email.getBody());

            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText(email.getBody());
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            messageBodyPart = new MimeBodyPart();
            String filename = "C:/Users/idrees/Desktop/license021.jpeg";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);
            // Send the complete message parts
            message.setContent(multipart);
            Transport.send(message);
            System.out.println("Email sent");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}

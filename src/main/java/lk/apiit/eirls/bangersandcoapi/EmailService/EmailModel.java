package lk.apiit.eirls.bangersandcoapi.EmailService;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailModel {

    @NotNull
    private String email;
    private String subject;
    private String body;
    private String imageurl;

    public EmailModel(@NotNull String email, String subject, String body) {
        this.email = email;
        this.subject = subject;
        this.body = body;
    }
}

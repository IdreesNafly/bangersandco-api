package lk.apiit.eirls.bangersandcoapi.EmailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mailservice/")
public class EmailController {

    @Autowired
    EmailService emailService;

    @PostMapping("/mailto")
    public boolean sendEmail(@RequestBody EmailModel emailModel){
        emailService.sendEmail(emailModel);
        return true;
    }

}

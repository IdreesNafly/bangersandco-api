package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.Authentication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends CrudRepository<Authentication,String> {
    public boolean existsByEmail(String email);

}

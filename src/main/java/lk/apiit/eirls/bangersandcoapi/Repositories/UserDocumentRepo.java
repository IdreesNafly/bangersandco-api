package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.User;
import lk.apiit.eirls.bangersandcoapi.Models.UserDocuments;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDocumentRepo extends CrudRepository<UserDocuments, Long> {

    UserDocuments findByFileName(String filename);
    List<UserDocuments> findAllByUser(User user);

}

package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.UserDocuments;
import org.springframework.data.repository.CrudRepository;

public interface FileRepo extends CrudRepository <UserDocuments,Integer> {

}

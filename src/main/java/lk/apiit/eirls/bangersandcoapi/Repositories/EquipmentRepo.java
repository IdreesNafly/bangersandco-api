package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.SpecialEquipment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EquipmentRepo extends CrudRepository<SpecialEquipment,Integer> {

//    @Query("select * from ")
//    List<SpecialEquipment> findAllByEquipmentId(List<Integer> list);

    List<SpecialEquipment> findSpecialEquipmentsByEquipmentId(int [] id);


}

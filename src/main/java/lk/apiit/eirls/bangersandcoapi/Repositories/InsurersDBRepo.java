package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.Insurer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsurersDBRepo extends CrudRepository <Insurer,Integer> {

    boolean existsByLicenseno(String licenseno);
}



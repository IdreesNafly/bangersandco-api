package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.Booking;
import lk.apiit.eirls.bangersandcoapi.Models.User;
import lk.apiit.eirls.bangersandcoapi.Models.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface BookingRepo extends CrudRepository <Booking,Integer> {

    List<Booking> findAllByUser(User user);

    List<Booking> findByVehicleAndBookingDateLessThanEqualAndReturnDateGreaterThanEqual(Vehicle vehicle, Date returnDate, Date bookingDate);

    List<Booking> findByVehicleAndReturnDateLessThanEqualAndExtendedDateGreaterThanEqual(Vehicle vehicle, Date extendDate, Date returnDate);

}
//
//    @Query("SELECT b from booking  b where  b.startDate >= :startDate and b.returnDate < :endDate")
//    Optional<Booking> checkAvailability(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

//    @Query("SELECT b from booking  b where b.vehicleId= :V_ID and b.startDate and b.returnDate between :startDate and :endDate")
//    List <Booking> checkAvailability2(@Param("startDate") Date startDate, @Param("endDate") Date endDate,@Param("V_ID") int vehicleId);

//}

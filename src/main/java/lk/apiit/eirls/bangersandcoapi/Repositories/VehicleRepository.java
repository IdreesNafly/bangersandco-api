package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends CrudRepository  <Vehicle,Integer> {

}

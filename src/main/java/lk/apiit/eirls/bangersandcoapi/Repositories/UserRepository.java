package lk.apiit.eirls.bangersandcoapi.Repositories;

import lk.apiit.eirls.bangersandcoapi.Models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User,String> {

}

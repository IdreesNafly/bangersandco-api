package lk.apiit.eirls.bangersandcoapi.Webscraper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class Webscraper  {

    public List <WebModel> scrapeData(){

        final String url ="http://www.srilankarentcar.com/Self-Driving-Rates";

        List <WebScrapingModel> webScrapingModels=new ArrayList<>();
        List <WebModel> webScrapingModels1=new ArrayList<>();

        Document document=null;
        try{
            document= Jsoup.connect(url).get();
        }catch(IOException e){
            System.out.println(e.getMessage());
        }

        Element tableElement= document.select("table").first();

        Elements rowElements=tableElement.select(":not(thead) tr");

        for(int i=2;i<rowElements.size();i++){
            Element row=rowElements.get(i);
            Elements rowItems=row.select("td");
            try {
                String name=rowItems.get(0).text();
                String pricePerWeek=rowItems.get(1).text();
                String pricePerMonth=rowItems.get(2).text();
                String week=pricePerWeek.replaceAll(",","");
                String month=pricePerMonth.replaceAll(",","");
                webScrapingModels.add(new WebScrapingModel(rowItems.get(0).text(),week,month));
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        }
        System.out.println(webScrapingModels.toArray());
//

        try {
            webScrapingModels1.add(new WebModel("MINI/SMALL TOWN CARS", Double.parseDouble(webScrapingModels.get(3).pricePerMonth), Double.parseDouble(webScrapingModels.get(3).pricePerWeek), Math.round(Double.parseDouble(webScrapingModels.get(3).pricePerWeek) / 7)));
            webScrapingModels1.add(new WebModel("ECONOMY/HATCHBACK", Double.parseDouble(webScrapingModels.get(4).pricePerMonth), Double.parseDouble(webScrapingModels.get(4).pricePerWeek), Math.round(Double.parseDouble(webScrapingModels.get(4).pricePerWeek) / 7)));
            webScrapingModels1.add(new WebModel("STANDARD/SALOON", Double.parseDouble(webScrapingModels.get(9).pricePerMonth), Double.parseDouble(webScrapingModels.get(9).pricePerWeek), Math.round(Double.parseDouble(webScrapingModels.get(9).pricePerWeek) / 7)));
            webScrapingModels1.add(new WebModel("SUV/ESTATE", Double.parseDouble(webScrapingModels.get(19).pricePerMonth), Double.parseDouble(webScrapingModels.get(19).pricePerWeek), Math.round(Double.parseDouble(webScrapingModels.get(19).pricePerWeek) / 7)));
            webScrapingModels1.add(new WebModel("MINI-VAN/VAN", Double.parseDouble(webScrapingModels.get(12).pricePerMonth), Double.parseDouble(webScrapingModels.get(12).pricePerWeek), Math.round(Double.parseDouble(webScrapingModels.get(12).pricePerWeek) / 7)));
        }
        catch(NumberFormatException nf){
            System.out.println(nf.getMessage());
        }

        return  webScrapingModels1;
    }



//        final String url ="https://www.rhinocarhire.com/USA.aspx#/searchcars";
//        List<String> image=new ArrayList<>();
//        List<String> vehicleName=new ArrayList<>();
//        List<String> Price=new ArrayList<>();
//        ArrayList<WebScrapingModel> webScrapingModels=new ArrayList<>();
//        try{
//            final Document document= Jsoup.connect(url).get();
//            Elements price=document.select("div.price");
//            Elements hTags = document.select("h3");
//            Elements images = document.select("div.carthum");
//            Elements img = document.getElementsByTag("img");
//            int i=0;
//            for (Element e:hTags
//            ) {
//                if(i<=4) {
//                    vehicleName.add(e.text());
//                }
//                i++;
//            }
//            int j=0;
//            for (Element e:price
//            ) {
//                if(j<=4) {
//                    Price.add(e.text());
//                }
//                j++;
//            }
//
//            for (Element e:images
//            ) {
//                Elements child=e.children().select("img");
//                for (Element ee:child
//                ) {
//                    if(ee.tagName().equals("img")){
//                        String src = ee.absUrl("src");
//                        image.add(src);
//                    }
//                }
//            }
//
//            for(int k=0;k<5;k++){
//                WebScrapingModel webScrapingModel =new WebScrapingModel(vehicleName.get(k),Price.get(k),image.get(k));
//                webScrapingModels.add(webScrapingModel);
//            }
//        }
//        catch(Exception e){
//            e.printStackTrace();
//
//        }
//        for(WebScrapingModel w:webScrapingModels
//        ) {
//            System.out.println(w.getName()+"   "+w.getPrice()+"   "+w.getUrl());
//        }
//        return webScrapingModels;
//    }
}

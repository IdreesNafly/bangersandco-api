package lk.apiit.eirls.bangersandcoapi.Webscraper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("api/webscrape/")
public class WebScraperController {

    @Autowired
    Webscraper webscraper;

    @GetMapping("fetch")
    public List<WebModel> getWebData(){
        return webscraper.scrapeData();
    }

}



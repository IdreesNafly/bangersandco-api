package lk.apiit.eirls.bangersandcoapi.Webscraper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WebModel {

    String name;
    double pricePerMonth;
    double pricePerWeek;
    double pricePerDay;

}

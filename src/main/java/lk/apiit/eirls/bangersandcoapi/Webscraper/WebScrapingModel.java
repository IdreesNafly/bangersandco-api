package lk.apiit.eirls.bangersandcoapi.Webscraper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WebScrapingModel {

//    private String Name;
//    private String Price;
//    private  String url;

    String name;
    String pricePerMonth;
    String pricePerWeek;
}

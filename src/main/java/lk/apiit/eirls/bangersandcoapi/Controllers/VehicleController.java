package lk.apiit.eirls.bangersandcoapi.Controllers;

import lk.apiit.eirls.bangersandcoapi.DTO.VehicleDTO;
import lk.apiit.eirls.bangersandcoapi.Models.Vehicle;
import lk.apiit.eirls.bangersandcoapi.Services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/vehicle")
@CrossOrigin("*")
public class VehicleController {
    private VehicleService vehicleService;
    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping("/allvehicle")
    public List <Vehicle> getAllVehicle(){
        return vehicleService.getAllVehicle();
    }

    @DeleteMapping("/deletevehicle/{id}")
    public ResponseEntity<Boolean> DeleteVehicle(@PathVariable int id) {
        return vehicleService.deleteVehicle(id);
    }

    @PostMapping("/addVehicle")
    public Boolean addvehicle(@RequestBody Vehicle vehicle){
        return vehicleService.addVehicle(vehicle);
    }

    @PutMapping("/updatevehicle")
    public Boolean updateVehicle(@RequestBody VehicleDTO vehicleDTO){
        return vehicleService.updateVehicle(vehicleDTO);
    }

    @GetMapping("/getvehicle/{id}")
    public Vehicle findVehicleById(@PathVariable int id){
        return vehicleService.findVehicleById(id);
    }


}

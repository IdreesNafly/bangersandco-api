package lk.apiit.eirls.bangersandcoapi.Controllers;

import lk.apiit.eirls.bangersandcoapi.Authentication.JWTTokenUtil;
import lk.apiit.eirls.bangersandcoapi.Exception.CustomException;
import lk.apiit.eirls.bangersandcoapi.Models.Authentication;
import lk.apiit.eirls.bangersandcoapi.Repositories.AuthRepository;
import lk.apiit.eirls.bangersandcoapi.Services.JWTUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin
public class JWTAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JWTTokenUtil jwtTokenUtil;

    @Autowired
    private JWTUserDetailsService jwtUserDetailsService;

    @Autowired
    AuthRepository authRepository;

    @PostMapping("/authenticate/")
    public ResponseEntity<Authentication> createAuthenticationToken(@RequestBody Authentication authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());

        final UserDetails userDetails = jwtUserDetailsService
                .loadUserByUsername(authenticationRequest.getEmail());

        final String token = jwtTokenUtil.generateToken(userDetails);

        Optional<Authentication> authenticationOptional = authRepository.findById(authenticationRequest.getEmail());
        if(authenticationOptional.isPresent()){
            Authentication authentication = authenticationOptional.get();
            authentication.setJwtToken(token);
            //Authentication authentication1 = new Authentication( authentication.getEmail(),authentication.getUserRole(),authentication.getLastLogin());
            return ResponseEntity.ok(authentication);
        }else{
            throw new CustomException("Email is not Registered!", HttpStatus.UNAUTHORIZED);
        }
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new CustomException("USER_DISABLED", HttpStatus.BAD_REQUEST);
        } catch (BadCredentialsException e) {
            throw new CustomException("INVALID_CREDENTIALS", HttpStatus.UNAUTHORIZED);
        }
    }
}

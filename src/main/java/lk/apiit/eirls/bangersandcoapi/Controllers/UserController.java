package lk.apiit.eirls.bangersandcoapi.Controllers;

import lk.apiit.eirls.bangersandcoapi.DTO.UserDTO;
import lk.apiit.eirls.bangersandcoapi.Models.User;
import lk.apiit.eirls.bangersandcoapi.Services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("api/user")
public class UserController {

    @Autowired
    UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @PostMapping("/signup")
    public ResponseEntity<Boolean> RegisterUser(@RequestBody User user){
        return userService.registerUser(user);
    }

    @GetMapping("/getuser/{email}")
    public ResponseEntity<User> getUserbyId(@PathVariable String email){
        return userService.getUser(email);
    }

    @GetMapping("/getalluser")
    public List<User> getAllVehicle(){
        return userService.getAllUser();
    }

    @DeleteMapping("/deleteuser/{id}")
    public ResponseEntity<?> DeleteUser(@PathVariable String id) {
        return userService.deleteUser(id);

    }

    @PutMapping("/updateuser")
    public Boolean UpdateUser(@RequestBody UserDTO user) {
        return userService.updateUser(user);
    }

    @PutMapping("/changepassword")
    public Boolean changePassword(UserDTO userDTO){
        return userService.changePassword(userDTO);
    }

    @PutMapping("/blacklistuser/{id}")
    public boolean blacklistUser(@PathVariable String id){
        return userService.blacklistUser(id);
    }
//    @PostMapping("/uploadFile")
//    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, String userid) {
//        UserDocuments dbFile = userService.storeFile(file,userid);
//
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                //.path(dbFile.getId())
//                .toUriString();
//
//        return new UploadFileResponse(dbFile.getFileName(), fileDownloadUri,
//                file.getContentType(), file.getSize());
//    }

//    @PostMapping("/uploadMultipleFiles")
//    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files, String userid) {
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//    }

//    @GetMapping("/downloadFile/{fileId}")
//    public ResponseEntity<Resource> downloadFile(@PathVariable int fileId) {
//        // Load file from database
//        UserDocuments dbFile = userService.getFile(fileId);
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
//                .body(new ByteArrayResource(dbFile.getData()));
//    }




}

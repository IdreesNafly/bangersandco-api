package lk.apiit.eirls.bangersandcoapi.Controllers;

import lk.apiit.eirls.bangersandcoapi.Models.SpecialEquipment;
import lk.apiit.eirls.bangersandcoapi.Services.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("api/equipments")
public class EquipmentController {

    private EquipmentService equipmentService;

    @Autowired
    public EquipmentController(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;
    }

    @GetMapping("/all")
    public List<SpecialEquipment> getAllEquipments(){
        return equipmentService.getAllEquipments();
    }

    @PostMapping("/add")
    public boolean addEquipement(@RequestBody SpecialEquipment specialEquipment){
        return equipmentService.AddEquipment(specialEquipment);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Boolean> deleteEquipment(@PathVariable int id){
        return equipmentService.deleteEquipment(id);
    }

    @GetMapping("/getbyid/{id}")
    public ResponseEntity<SpecialEquipment> getEquipmentById(@PathVariable int id){
        return equipmentService.getEquipment(id);
    }

    @PutMapping("/update/{id}")
    public boolean updateEquipment(@RequestBody SpecialEquipment specialEquipment, @PathVariable int id){
        return equipmentService.updateEquipment(specialEquipment,id);
    }

    @GetMapping("/getbyIds")
    public List<SpecialEquipment> getAllEquipmentbyIds(@RequestBody int [] id){
        return equipmentService.findAllbyIds(id);
    }

}

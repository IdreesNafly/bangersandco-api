package lk.apiit.eirls.bangersandcoapi.Controllers;

import lk.apiit.eirls.bangersandcoapi.DTO.BookingDTO;
import lk.apiit.eirls.bangersandcoapi.Models.Booking;
import lk.apiit.eirls.bangersandcoapi.Models.User;
import lk.apiit.eirls.bangersandcoapi.Repositories.BookingRepo;
import lk.apiit.eirls.bangersandcoapi.Repositories.InsurersDBRepo;
import lk.apiit.eirls.bangersandcoapi.Repositories.UserRepository;
import lk.apiit.eirls.bangersandcoapi.Services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("api/booking")
public class BookingController {

    @Autowired
    BookingService bookingService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookingRepo bookingRepo;

    @Autowired
    InsurersDBRepo insurersDBRepop;

    @GetMapping("/insu/{id}")
    public boolean insu(@PathVariable String id){
        return insurersDBRepop.existsByLicenseno(id);
    }

    @PostMapping("/addBooking")
    public ResponseEntity<?> AddBooking(@RequestBody BookingDTO bookingDTO) throws Exception{
        return bookingService.addBooking(bookingDTO);
    }

    @GetMapping("/getBooking/{userid}")
    public List <BookingDTO> getAllBookingByUser(@PathVariable String userid){
        Optional<User> user=userRepository.findById(userid);
        User user1=user.get();
        return bookingService.getAllBookingByUser(user1);
    }

    @DeleteMapping("/deleteBooking/{id}")
    public Boolean deleteBooking(@PathVariable int id){
        return bookingService.deleteBooking(id);
    }

    @PutMapping("/updateStatus/{id}")
    public Boolean updateStatus(@PathVariable int id,@RequestBody BookingDTO bookingDTO){
        return bookingService.updateStatus(id,bookingDTO);
    }

    @GetMapping("/getAllBooking")
    public List<BookingDTO> getAllBooking(){
        return bookingService.getAllBooking();
    }

    @GetMapping("/getbookingbyid/{id}")
    public Optional<Booking> getBookingById(@PathVariable int id){
        return bookingService.getBookingById(id);
    }
//
//    @DeleteMapping("/deleteuser/{id}")
//    public ResponseEntity<Boolean> DeleteUser(@PathVariable String id) {
//        return userService.deleteUser(id);
//    }
//
//    @PutMapping("/updateuser/{id}")
//    public ResponseEntity<User> UpdateUser(@PathVariable String id, @RequestBody User user) {
//        return userService.updateUser(id,user);
//    }

    @PutMapping("/extendbooking")
    public ResponseEntity<?>extendBooking(@RequestBody BookingDTO bookingDTO){
        return bookingService.extendBooking(bookingDTO);
    }

    @GetMapping("/calcPayment/{id}")
    public ResponseEntity<?> calcPayment(@PathVariable int id){
        return bookingService.calcPayment(id);
    }
    @Autowired
    InsurersDBRepo insurersDBRepo;

//    @GetMapping("/fraudclaims/{id}")
//    public Insurer checkclaims(@PathVariable String id){
//        String licenseno="LI0001";
////        String user="test123@gmail.com";
//        Insurer insurer =insurersDBRepo.findByLicenseno("LI0001");
//
//        return insurer;
//    }
}

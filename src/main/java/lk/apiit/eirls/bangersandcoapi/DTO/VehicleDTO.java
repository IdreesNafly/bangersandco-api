package lk.apiit.eirls.bangersandcoapi.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VehicleDTO {
    int vehicleid;
    private Float vehicleRate;
    private boolean availability=true;

    private String fuelType;

    private String transmission;

    private String mileage;

    private String vehicleMake;

    private String vehicleModel;

    private String YOM;

    public VehicleDTO(int vehicleid, Float vehicleRate) {
        this.vehicleid = vehicleid;
        this.vehicleRate = vehicleRate;
    }

    public VehicleDTO(int vehicleid, Float vehicleRate, String fuelType, String transmission, String mileage, String vehicleMake, String vehicleModel, String YOM) {
        this.vehicleid = vehicleid;
        this.vehicleRate = vehicleRate;
        this.fuelType = fuelType;
        this.transmission = transmission;
        this.mileage = mileage;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.YOM = YOM;
    }
}

package lk.apiit.eirls.bangersandcoapi.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDocDto {

    private String useremail;

    private Date validTill;

    private String docTitle;

    private String docNo;
}

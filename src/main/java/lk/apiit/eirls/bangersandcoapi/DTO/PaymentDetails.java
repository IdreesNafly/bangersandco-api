package lk.apiit.eirls.bangersandcoapi.DTO;

import lk.apiit.eirls.bangersandcoapi.Models.SpecialEquipment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PaymentDetails {
    double totalamount;
    int totalBookingHours;
    Date startDate;
    Date returnDate;
    Date extendedDate;
    double totalVehicleRate;
    double totalEquipmentRate;
    String vehicleType;
    String vehicleModel;
    String useremail;
    String username;
    float vehiclerate;
    double equipmentrate;
    int noOfEquipments;
    int extendedHours;
    List<SpecialEquipment> specialEquipmentList;


}

package lk.apiit.eirls.bangersandcoapi.DTO;

import lk.apiit.eirls.bangersandcoapi.Enums.BookingStatus;
import lombok.*;

import java.util.Date;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class BookingDTO {
    private int bookingID;
    private Date startDate;
    private Date endDate;
    private Date extendDate;
    private String userid;
    private int vehicleId;
    private BookingStatus bookingStatus;
    private List<Integer> equipmentId;

//    public BookingDTO(Date startDate, Date endDate, String userid, int vehicleId, BookingStatus bookingStatus,int bookingID) {
//        this.startDate = startDate;
//        this.endDate = endDate;
//        this.userid = userid;
//        this.vehicleId = vehicleId;
//        this.bookingStatus = bookingStatus;
//        this.bookingID=bookingID;
//    }

    public BookingDTO(Date startDate, Date endDate, String userid, int vehicleId, BookingStatus bookingStatus, List<Integer> equipmentId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.userid = userid;
        this.vehicleId = vehicleId;
        this.bookingStatus = bookingStatus;
        this.equipmentId = equipmentId;
    }

    public BookingDTO(int bookingID, Date startDate, Date endDate, Date extendDate, int vehicleId, List<Integer> equipmentId) {
        this.bookingID = bookingID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.extendDate = extendDate;
        this.vehicleId = vehicleId;
        this.equipmentId = equipmentId;
    }

    public BookingDTO(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }
}

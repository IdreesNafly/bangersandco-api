package lk.apiit.eirls.bangersandcoapi.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    @Id
    private String email;
    private String password;
    private String token;

    private String address;

    private String contactNo;

    private boolean isNewUser=true;

    private boolean isBacklisted;

    public UserDTO(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public UserDTO(String email, String address, String contactNo) {
        this.email = email;
        this.address = address;
        this.contactNo = contactNo;
    }

    public UserDTO(String email, boolean isNewUser, boolean isBacklisted) {
        this.email = email;
        this.isNewUser = isNewUser;
        this.isBacklisted = isBacklisted;
    }

    public UserDTO(String token) {
        this.token = token;
    }
}

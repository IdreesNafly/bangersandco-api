package lk.apiit.eirls.bangersandcoapi;

import lk.apiit.eirls.bangersandcoapi.FileUpload.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EnableConfigurationProperties(StorageProperties.class)
public class BangersandcoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BangersandcoApiApplication.class, args);
        //        DMVIntegration dmvIntegration=new DMVIntegration();
//        dmvIntegration.validateLicense("LC0111");
//
//        EmailModel emailModel=new EmailModel();
//        emailModel.setBody("Test123");
//        emailModel.setEmail("idreesna@hotmail.com");
//        emailModel.setSubject("Test");
//
//        EmailService emailService=new EmailService();
//        emailService.sendEmail(emailModel);
    }

}

package lk.apiit.eirls.bangersandcoapi.Services;

import lk.apiit.eirls.bangersandcoapi.Exception.CustomException;
import lk.apiit.eirls.bangersandcoapi.Models.SpecialEquipment;
import lk.apiit.eirls.bangersandcoapi.Repositories.EquipmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EquipmentService {

    private EquipmentRepo equipmentRepo;

    @Autowired
    public EquipmentService(EquipmentRepo equipmentRepo) {
        this.equipmentRepo = equipmentRepo;
    }

    public List getAllEquipments(){
        List <SpecialEquipment> equipmentList=new ArrayList<>();
        equipmentRepo.findAll().forEach(equipment->equipmentList.add(equipment));
        return equipmentList;
    }

    public Boolean AddEquipment(SpecialEquipment specialEquipment){
        Optional <SpecialEquipment> specialEquipment1=equipmentRepo.findById(specialEquipment.getEquipmentId());

        if(!specialEquipment1.isPresent()){
            equipmentRepo.save(specialEquipment);
            return true;
        }
        return false;
    }

    public ResponseEntity<Boolean> deleteEquipment(int id) {
            equipmentRepo.deleteById(id);
            return new ResponseEntity<>(true, HttpStatus.OK);
    }

    public Boolean updateEquipment(SpecialEquipment specialEquipment, int id){
        Optional <SpecialEquipment> equipment=equipmentRepo.findById(id);

        if(equipment.isPresent()){
            SpecialEquipment specialEquipment1=equipment.get();
            specialEquipment1.setCostPerHour(specialEquipment.getCostPerHour());
            equipmentRepo.save(specialEquipment1);
            return true;
        }
        return false;
    }

    public ResponseEntity<SpecialEquipment> getEquipment(int id) {
        Optional<SpecialEquipment> specialEquipment = equipmentRepo.findById(id);
        if (specialEquipment.isPresent()) {
            SpecialEquipment specialEquipment1 = specialEquipment.get();
            return new ResponseEntity<>(specialEquipment1, HttpStatus.OK);
        }
        throw new CustomException("Equipment not found with id", HttpStatus.NOT_FOUND);
    }

    public List <SpecialEquipment> getSpecialEquipment(List <Integer> ids){
        List <SpecialEquipment> specialEquipmentList = new ArrayList<>();
        ids.forEach(id->{
            Optional<SpecialEquipment> specialEquipment=equipmentRepo.findById(id);
            SpecialEquipment specialEquipment1=specialEquipment.get();
            specialEquipmentList.add(specialEquipment1);
        });
        return specialEquipmentList;
    }

    public List<SpecialEquipment> findAllbyIds(int[] id) {
        List <SpecialEquipment> specialEquipmentList=new ArrayList<>();
        if(id.length>0){
            for (int i = 0; i < id.length; i++) {
                int x = id[i];

                Optional<SpecialEquipment> specialEquipment=equipmentRepo.findById(x);
                SpecialEquipment specialEquipment1=specialEquipment.get();
                specialEquipmentList.add(specialEquipment1);
            }
        }
        return specialEquipmentList;
    }

    //getequipment by booking id
    //get available equipment



}

package lk.apiit.eirls.bangersandcoapi.Services;

import lk.apiit.eirls.bangersandcoapi.DTO.UserDTO;
import lk.apiit.eirls.bangersandcoapi.Enums.UserRole;
import lk.apiit.eirls.bangersandcoapi.Exception.CustomException;
import lk.apiit.eirls.bangersandcoapi.Models.Authentication;
import lk.apiit.eirls.bangersandcoapi.Models.User;
import lk.apiit.eirls.bangersandcoapi.Repositories.AuthRepository;
import lk.apiit.eirls.bangersandcoapi.Repositories.FileRepo;
import lk.apiit.eirls.bangersandcoapi.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthRepository authRepository;

    @Autowired
    FileRepo fileRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public ResponseEntity<Boolean> registerUser(User user) {

        Optional <Authentication> authenticationOptional=authRepository.findById(user.getEmail());

        if(!authenticationOptional.isPresent()){
            Authentication authentication=user.getAuthentication();
            authentication.setPassword(passwordEncoder.encode(authentication.getPassword()));
            Date date=new Date();
            authentication.setLastLogin(date);
            authentication.setUserRole(UserRole.Customer);
            authentication=authRepository.save(authentication);

            user.setAuthentication(authentication);
            User newUser=userRepository.save(user);

            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        //return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        throw new CustomException("User is Already Regsistered!",HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<User> getUser(String id) {
        Optional<User> userOptinal = userRepository.findById(id);
        if (userOptinal.isPresent()) {
            User user = userOptinal.get();
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
       // return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        throw new CustomException("User not Found!", HttpStatus.UNAUTHORIZED);
    }

    public ResponseEntity<?>deleteUser(String id) {
        Optional<User> user=userRepository.findById(id);
            if(user.isPresent()){
                userRepository.deleteById(id);
                return new ResponseEntity<>(true, HttpStatus.OK);
            }else{
                throw new CustomException("User ID not found!", HttpStatus.UNAUTHORIZED);
            }
    }

    public Boolean updateUser(UserDTO userDTO) {
        Optional<User> userOptional = userRepository.findById(userDTO.getEmail());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setContactNo(userDTO.getContactNo());
            user.setAddress(userDTO.getAddress());
            userRepository.save(user);
            return true;
        }
            return false;
    }

    public Boolean changePassword(UserDTO userDTO){
        Optional<User> userOptional=userRepository.findById(userDTO.getEmail());
        Optional <Authentication> authenticationOptional=authRepository.findById(userDTO.getEmail());

        if(userOptional.isPresent() && authenticationOptional.isPresent()){
            User user=userOptional.get();
            Authentication authentication=authenticationOptional.get();

            authentication.setPassword(userDTO.getPassword());
            user.setAuthentication(authentication);
            authRepository.save(authentication);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public List getAllUser(){
        List <User> userList=new ArrayList<>();
        userRepository.findAll().forEach(user->userList.add(user));
        return userList;
    }

    public boolean blacklistUser(String id) {
        Optional<User> user=userRepository.findById(id);

        if(user.isPresent()){
            User user1=user.get();

            if(user1.isBacklisted()){
                user1.setBacklisted(false);
            }else if(user1.isBacklisted()==false){
                user1.setBacklisted(true);
            }
            userRepository.save(user1);
            return true;
        }
        return false;
    }

//    public UserDocuments storeFile(MultipartFile file, String userId) {
//        Optional <User> userOptional=userRepository.findById(userId);
//        User user=userOptional.get();
//
//        // Normalize file name
//        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//
//        try {
//            // Check if the file's name contains invalid characters
//            if(fileName.contains("..")) {
//                //throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
//            }
//
//            UserDocuments userDocuments = new UserDocuments(user,fileName, file.getContentType(), file.getBytes());
//
//            return fileRepo.save(userDocuments);
//        } catch (IOException ex) {
//            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
//        }
//    }

//    public UserDocuments getFile(int id) {
//        return fileRepo.findById(id)
//                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + id));
//    }


    //BacklistUser
    //isNewUser


}

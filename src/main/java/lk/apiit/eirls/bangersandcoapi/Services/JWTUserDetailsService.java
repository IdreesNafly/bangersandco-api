package lk.apiit.eirls.bangersandcoapi.Services;

import lk.apiit.eirls.bangersandcoapi.Models.Authentication;
import lk.apiit.eirls.bangersandcoapi.Repositories.AuthRepository;
import lk.apiit.eirls.bangersandcoapi.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class JWTUserDetailsService implements UserDetailsService {

    @Autowired
    AuthRepository authRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Authentication> optionalAuthentication=authRepository.findById(email);

        if(optionalAuthentication.isPresent()){
            Authentication authentication=optionalAuthentication.get();
            return new User(authentication.getEmail(),authentication.getPassword(),new ArrayList<>());
        }
        throw new UsernameNotFoundException("Username not found with username: "+email);
    }

}

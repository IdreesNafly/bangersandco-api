package lk.apiit.eirls.bangersandcoapi.Services;

import lk.apiit.eirls.bangersandcoapi.DTO.VehicleDTO;
import lk.apiit.eirls.bangersandcoapi.Models.Vehicle;
import lk.apiit.eirls.bangersandcoapi.Repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VehicleService {

   private VehicleRepository vehicleRepository;

    @Autowired
    public VehicleService(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    public List getAllVehicle(){
        List <Vehicle> vehicleList=new ArrayList<>();
        vehicleRepository.findAll().forEach(vehicle->vehicleList.add(vehicle));
        return vehicleList;
    }

    public ResponseEntity<Boolean> deleteVehicle(int id) {
        try {
            vehicleRepository.deleteById(id);
            return new ResponseEntity("Successfully Deleted", HttpStatus.OK);
        } catch (EmptyResultDataAccessException erda_ex) {

        }
        return new ResponseEntity("Error", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public Boolean addVehicle(Vehicle vehicle){
        Optional<Vehicle> vehicleOptional=vehicleRepository.findById(vehicle.getVehicleID());

       if(!vehicleOptional.isPresent()){
           vehicleRepository.save(vehicle);
           return true;
       }
       return false;
    }

    public Boolean updateVehicle(VehicleDTO vehicleDTO){
        //return new ResponseEntity<>(true)
        Optional <Vehicle> vehicle=vehicleRepository.findById(vehicleDTO.getVehicleid());
        if(vehicle.isPresent())
        {
            Vehicle vehicle1=vehicle.get();
            vehicle1.setVehicleRate(vehicleDTO.getVehicleRate());
            vehicleRepository.save(vehicle1);

            return true;
        }
            return false;
    }

    public Vehicle findVehicleById(int id){
        Optional<Vehicle> vehicle= vehicleRepository.findById(id);
        Vehicle vehicle1=vehicle.get();

        return vehicle1;
    }



}

package lk.apiit.eirls.bangersandcoapi.Services;


import lk.apiit.eirls.bangersandcoapi.DMVIntegration.DMV;
import lk.apiit.eirls.bangersandcoapi.DMVIntegration.DMVIntegration;
import lk.apiit.eirls.bangersandcoapi.DTO.BookingDTO;
import lk.apiit.eirls.bangersandcoapi.DTO.PaymentDetails;
import lk.apiit.eirls.bangersandcoapi.EmailService.EmailModel;
import lk.apiit.eirls.bangersandcoapi.EmailService.EmailService;
import lk.apiit.eirls.bangersandcoapi.Enums.BookingStatus;
import lk.apiit.eirls.bangersandcoapi.Exception.CustomException;
import lk.apiit.eirls.bangersandcoapi.Models.*;
import lk.apiit.eirls.bangersandcoapi.Repositories.*;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookingService {

    //Add Booking

    @Autowired
    BookingRepo bookingRepo;
    @Autowired
    UserRepository userRepository;
    @Autowired
    EquipmentRepo equipmentRepo;
    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    EquipmentService equipmentService;

    @Autowired
    UserDocumentRepo userDocumentRepo;

    @Autowired
    DMVIntegration dmvIntegration;

    @Autowired
    EmailService emailService;

    @Autowired
    InsurersDBRepo insurersDBRepo;

    public ResponseEntity<?>addBooking(BookingDTO bookingDTO){
        Optional <User> user=userRepository.findById(bookingDTO.getUserid());
        Optional <Vehicle> vehicle=vehicleRepository.findById(bookingDTO.getVehicleId());
        User user1=user.get();
        Vehicle vehicle1=vehicle.get();
        List <SpecialEquipment> specialEquipment=new ArrayList<>();
        specialEquipment=equipmentService.getSpecialEquipment(bookingDTO.getEquipmentId());

        List <UserDocuments> userDocuments=new ArrayList<>();

        userDocumentRepo.findAllByUser(user1).forEach(userDocuments1 -> userDocuments.add(userDocuments1));

        if(user1.isBacklisted()) {
            throw new CustomException("You have been blacklisted by the Admin. Please Contact Admin to resolve issue!",HttpStatus.BAD_REQUEST);
        }
            if(userDocuments.size()<=0){
            System.out.println("Please add a document to proceed with the booking");
            throw new CustomException("Please add a document to proceed with the booking",HttpStatus.BAD_REQUEST);
        }
        //check if license suspended and send a maiil
        String LicenseNo="";
            String imageurl="";
        for (UserDocuments userDoc : userDocuments){
            if(userDoc.getDocTitle().equals("License")){
                LicenseNo=userDoc.getDocNo();
                imageurl=userDoc.getUrl();
                System.out.println("License No: "+LicenseNo);
            }
        }
        DMV dmv=dmvIntegration.validateLicense(LicenseNo);

        if(dmv!=null){
            System.out.println("License Not Valid");
            EmailModel emailModel=new EmailModel();
            emailModel.setSubject("BangerAndCo: License Not Valid");
            emailModel.setEmail("idreesna@hotmail.com");
            String body="The Licence bearing Identication Number : "+dmv.getLicenseno()+" has been "+dmv.getType()+". The date and time of the Offense is: "+dmv.getOffenseDate()+" Thank You!";
            emailModel.setBody(body);
            emailModel.setImageurl(imageurl);
            emailService.sendEmail(emailModel);
            throw  new CustomException("License is Invalid! An Email has been sent to you for more details", HttpStatus.BAD_REQUEST);
        }

        boolean isFraudClaims=false;
        isFraudClaims=insurersDBRepo.existsByLicenseno(LicenseNo);

        if(isFraudClaims){
            if(userDocuments.size()==1){
                throw new CustomException("Please Add Another form of Identification Document. Cannot book Vehicle!",HttpStatus.BAD_REQUEST);
            }
            throw new CustomException("Your license has record for Fraudulent Claims. Cannot book Vehicle!",HttpStatus.BAD_REQUEST);
        }

        List <Booking> bookings =bookingRepo.findByVehicleAndBookingDateLessThanEqualAndReturnDateGreaterThanEqual(vehicle1,bookingDTO.getEndDate(),bookingDTO.getStartDate());
        Booking booking = new Booking();
        Boolean flag=true;
        if(bookings.size()>0){
            System.out.println("Vehicle is not Available for the selected Date");
            flag=false;
            throw  new CustomException("Vehicle is not Available for the selected Date and Time",HttpStatus.BAD_REQUEST);
        }
        List <Booking> bookingscount=bookingRepo.findAllByUser(user1);
        if(bookingscount.size()>0){
            user1.setNewUser(false);
        }

        if(user.isPresent() && flag){
                booking.setVehicle(vehicle1);
                booking.setBookingStatus(BookingStatus.Booking_Confirmed);
                booking.setBookingDate(bookingDTO.getStartDate());
                booking.setReturnDate(bookingDTO.getEndDate());
                booking.setUser(user1);
                booking.setSpecialEquipment(specialEquipment);
                bookingRepo.save(booking);
                return new ResponseEntity<>("Booking Success", HttpStatus.OK);

        }

        throw  new CustomException("Error. Please try again!",HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public Boolean deleteBooking(int id){
        bookingRepo.deleteById(id);
        return true;
    }

    public Optional<Booking> getBookingById(int id){
        return bookingRepo.findById(id);
    }

    public List getAllBooking(){
        List <BookingDTO> bookingDTOS=new ArrayList<>();
        for (Booking booking : bookingRepo.findAll()) {
            BookingDTO bookingDTO=new BookingDTO();
            User user = booking.getUser();
            List<SpecialEquipment> specialEquipment = booking.getSpecialEquipment
                    ();
            List <Integer> equipmentIds = new ArrayList<>();
            specialEquipment.forEach(specialEquipment1 -> {
                        int equipmentid=specialEquipment1.getEquipmentId();
                        equipmentIds.add(equipmentid);
                    }
            );
            Vehicle vehicle=booking.getVehicle();
            bookingDTO.setUserid(user.getEmail());
            bookingDTO.setEquipmentId(equipmentIds);
            bookingDTO.setEndDate(booking.getReturnDate());
            bookingDTO.setStartDate(booking.getBookingDate());
            bookingDTO.setBookingStatus(booking.getBookingStatus());
            bookingDTO.setVehicleId(vehicle.getVehicleID());
            bookingDTO.setBookingID(booking.getId());
            bookingDTOS.add(bookingDTO);
        }
        return bookingDTOS;
    }

    public List <BookingDTO> getAllBookingByUser(User user){
        List <BookingDTO> bookingDTOS=new ArrayList<>();
        try {
            for (Booking booking : bookingRepo.findAllByUser(user)) {
                BookingDTO bookingDTO = new BookingDTO();
                List<SpecialEquipment> specialEquipment = booking.getSpecialEquipment
                        ();
                List<Integer> equipmentIds = new ArrayList<>();
                specialEquipment.forEach(specialEquipment1 -> {
                            int equipmentid = specialEquipment1.getEquipmentId();
                            equipmentIds.add(equipmentid);
                        }
                );
                Vehicle vehicle = booking.getVehicle();
                bookingDTO.setEquipmentId(equipmentIds);
                bookingDTO.setEndDate(booking.getReturnDate());
                bookingDTO.setStartDate(booking.getBookingDate());
                bookingDTO.setBookingStatus(booking.getBookingStatus());
                bookingDTO.setVehicleId(vehicle.getVehicleID());
                bookingDTO.setExtendDate(booking.getExtendedDate());
                bookingDTO.setBookingID(booking.getId());
                bookingDTOS.add(bookingDTO);
                //BookingDTO bookingDTO = new BookingDTO(booking.getId(),booking.getBookingDate(),booking.getReturnDate(),booking.getExtendedDate(),vehicle.getVehicleID(),equipmentIds);

            }
        }catch(Exception ex){
            ex.getMessage();
        }
        return bookingDTOS;
    }

    public Boolean updateStatus(int id, BookingDTO bookingDTO){
        Optional <Booking> booking=bookingRepo.findById(id);

        if(booking.isPresent()){
            Booking booking1=booking.get();
            //BookingStatus status1=BookingStatus.valueOf(status);
            BookingStatus status=bookingDTO.getBookingStatus();
            booking1.setBookingStatus(status);
            bookingRepo.save(booking1);

            return true;
        }
        //booking.setBookingStatus(BookingStatus.valueOf(status));
        return false;
    }

    public ResponseEntity<?> extendBooking(BookingDTO bookingDTO){

        Optional<Booking> booking=bookingRepo.findById(bookingDTO.getBookingID());
        if(booking.isPresent()){
            Booking bookingobj=booking.get();
            Optional <User> userOptional=userRepository.findById(bookingobj.getUser().getEmail());
            User user=userOptional.get();

            if(user.isNewUser()){
                throw  new CustomException("New users cannot extend booking", HttpStatus.BAD_REQUEST);
            }

            Vehicle vehicle=bookingobj.getVehicle();
            Date extendDate=bookingDTO.getExtendDate();

            List <Booking> bookings =bookingRepo.findByVehicleAndReturnDateLessThanEqualAndExtendedDateGreaterThanEqual(vehicle,extendDate,bookingobj.getReturnDate());

            if(bookings.size()>0){
                throw  new CustomException("Vehicle is not available on the following day for Extension!",HttpStatus.BAD_REQUEST);
            }
            bookingobj.setExtendedDate(bookingDTO.getExtendDate());
            bookingRepo.save(bookingobj);
            return new ResponseEntity<>("Booking extended Successfully",HttpStatus.OK);
//            bookingRepo.findByVehicleAndBookingDateLessThanEqualAndReturnDateGreaterThanEqual(vehicle,"",bookingobj.getReturnDate());
        }
        throw  new CustomException("Extension Failed!",HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<?> calcPayment(int id) {
        Optional <Booking> bookingOptional=bookingRepo.findById(id);

        Booking booking=bookingOptional.get();

        Vehicle vehicle=booking.getVehicle();
        List <SpecialEquipment> specialEquipment=booking.getSpecialEquipment();

        int noOfDays=0;
        int hours=0;
        Float vehicleRate=vehicle.getVehicleRate();
         //Float equipmentRate = 0.0f;

        double equipmentRate= specialEquipment.stream().mapToDouble(SpecialEquipment::getCostPerHour).sum();
        double totalAmount=0;

        DateTime startdate = new DateTime(booking.getBookingDate());
        DateTime endDate=new DateTime(booking.getReturnDate());

        int extendedHours=0;
        if(booking.getExtendedDate()!=null){
            DateTime extendedDate=new DateTime(booking.getExtendedDate());
            Period p = new Period(startdate,extendedDate);
            hours = p.getHours();
            Period pp=new Period(endDate,extendedDate);
            extendedHours=pp.getHours();
        }
        else {
            Period p = new Period(startdate,endDate);
            hours = p.getHours();
        }
        double ratePerHour=vehicleRate/10;
        double totalVehicleRate=hours*ratePerHour;
        double totalEquipmentrate=hours*equipmentRate;

        totalAmount=totalVehicleRate+totalEquipmentrate;
        PaymentDetails paymentDetails=new PaymentDetails();
        paymentDetails.setTotalamount(totalAmount);
        paymentDetails.setTotalEquipmentRate(totalEquipmentrate);
        paymentDetails.setTotalVehicleRate(totalVehicleRate);
        paymentDetails.setVehicleModel(vehicle.getVehicleMake()+" "+vehicle.getVehicleModel());
        paymentDetails.setVehicleType(vehicle.getVehicleType());
        paymentDetails.setUseremail(booking.getUser().getEmail());
        paymentDetails.setUsername(booking.getUser().getFirstName()+" "+booking.getUser().getLastName());
        paymentDetails.setSpecialEquipmentList(booking.getSpecialEquipment());
        paymentDetails.setStartDate(booking.getBookingDate());
        paymentDetails.setExtendedDate(booking.getExtendedDate());
        paymentDetails.setReturnDate(booking.getReturnDate());
        paymentDetails.setTotalBookingHours(hours);
        paymentDetails.setVehiclerate(vehicleRate);
        paymentDetails.setEquipmentrate(equipmentRate);
        paymentDetails.setNoOfEquipments(specialEquipment.size());
        paymentDetails.setExtendedHours(extendedHours);
        return new ResponseEntity<>(paymentDetails,HttpStatus.OK);
    }
    //DeleteBooking

    //UpdateBooking

    //CheckAvailability

    ///ViewbookingbyID

    //ViewAllBookings

    //ExtendBooking

}

package lk.apiit.eirls.bangersandcoapi.DMVIntegration;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DMV {

    private int id;
    private String licenseno;
    private String type;
    private String offenseDate;
}

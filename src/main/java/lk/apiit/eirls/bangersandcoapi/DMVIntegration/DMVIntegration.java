package lk.apiit.eirls.bangersandcoapi.DMVIntegration;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class DMVIntegration {

    public static DMV validateLicense(String licenseno) {

        final String uri = "http://localhost:8081/dmv/api/validateLicence/{licenseNo}";

        Map<String, String> params = new HashMap<String, String>();

        params.put("licenseNo", licenseno);

        RestTemplate restTemplate = new RestTemplate();

        DMV dmv = restTemplate.getForObject(uri, DMV.class, params);

        return dmv;
    }


}

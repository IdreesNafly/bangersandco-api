package lk.apiit.eirls.bangersandcoapi.Exception;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorResponse{

    private String message;

}
package lk.apiit.eirls.bangersandcoapi.Enums;

public enum BookingStatus {
    Booking_Confirmed,
    Picked_Up,
    Cancelled,
    Extended,
    InProgress,
    Completed
}
